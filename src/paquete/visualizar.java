/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package paquete;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import paquete.Orden;
import paquete.Lineaorden;
import paquete.Producto;
/**
 *
 * @author Lore-Lap
 */
public class visualizar implements Serializable{
    EntityManagerFactory emf=Persistence.createEntityManagerFactory("muchosAmuchosPU");
            EntityManager em=emf.createEntityManager();
            private static Collection<Lineaorden> lista_lineaOrden;
            
            public void  visualizar_orden(){
            em.getTransaction().begin();
            Collection<Orden> lista_orden=em.createNamedQuery("Orden.findAll").getResultList();          
            for(Orden o: lista_orden){
                System.out.println("id: "+o.getIdOrden());
                System.out.println("fecha "+o.getFechaOrden());
            }            
           // em.persist(lista_orden);
            em.close();
            emf.close();                                    
            }
           
            public void  visualizar_producto(){
            em.getTransaction().begin();
            Collection<Producto> lista_producto=em.createNamedQuery("Producto.findAll").getResultList();           
            for(Producto p: lista_producto){
                System.out.println("id: "+p.getIdProducto());
                System.out.println("descripcion: "+p.getDescripcion());
                System.out.println("precio "+p.getPrecio());
            }                        
           // em.persist(lista_producto);
            em.close();
            emf.close();                       
            }
             
    /**
     *
     */
    public void  visualizar_lineaOrden(){
           try{ 
            em.getTransaction().begin();
            lista_lineaOrden=em.createNamedQuery("Lineaorden.findAll").getResultList();          
            
            for(Lineaorden lo: lista_lineaOrden){                              
                    System.out.println("orden: "+lo.getOrden().getIdOrden());                                        
                    System.out.println("producto: "+lo.getProducto().getDescripcion());
                    System.out.println("cantidad: "+lo.getCantidad());
                }em.getTransaction().commit();
           }catch (Exception e){
               System.out.println(e);
           }
            em.close();
            emf.close();                         
            }
    }
